# Muograph (POC)

This project is a proof of concept that depicts the possibilities of using CQL and GraphQL hand-in-hand.

## Project Structure

| File | Use |
|---|---|
| `src/index.js` | The source code
| `src/schemas/main.gql` | The GraphQL schematic

## Dependencies

To run this code, you will need to install `Node.js`.

This project uses:
* `apollo-server-express` (GraphQL)
* `cassandra-driver` (Cassandra)
* `cors` (Allow GraphQL Requests)
* `express` (Server)
* `graphql` (GraphQL)

Because Muograph is written in node, a simple `npm install` will install all of these dependencies in one go!

## Getting Started

To get started, follow these simple steps

1. Clone or download this repo
2. Run `npm install` or `yarn install` (depending on your preference)
3. Run `node src/index.js`
4. Go to `http://localhost:4000/graphql` and run your GraphQL code

## License

MIT
