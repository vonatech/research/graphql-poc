const fs = require("fs");
const path = require("path");
const express = require("express");
const cql = require("cassandra-driver");
const { ApolloServer, gql } = require("apollo-server-express");

const cors = require("cors");

var client = new cql.Client({
	keyspace: "example",
	contactPoints: ["localhost"],
	localDataCenter: "datacenter1"
});

const QUERIES = {

	checkIfUniqueUsername: "SELECT * FROM users WHERE username=?;",
	checkIfUniqueEmail: "SELECT * FROM users WHERE email=?;",
	getUserByUsername: "SELECT * FROM users WHERE username=?;",
	getUserByEmail: "SELECT * FROM users WHERE email=?;",
	newUser: "INSERT INTO users (username, email, password) VALUES (?, ?, ?);"

}

// Construct a schema, using GraphQL schema language
const typeDefs = gql`${fs.readFileSync(path.join(__dirname, "schemas/main.gql")).toString()}`;

async function user (username, email, password) {

	var q;
	if (username) q = QUERIES.getUserByUsername
	else if (email) q = QUERIES.getUserByEmail

	var data = (await client.execute(q, [username ? username : email])).rows[0];

	if (password) {

		if (data.password !== password) return;
		else return data;

	} else return data;

}

// Provide resolver functions for your schema fields
const resolvers = {

	Query: {

		user: async (_, { username, password }) => {

			if (username && password)
				return user(username, undefined, password);
			if (username)
				return user(username);

		}

	},

	Mutation: {

		newUser: async (_, { user }) => {

			try {

				if ((await client.execute(QUERIES.checkIfUniqueUsername, [user.username])).rowLength !== 0) throw "USER_ALREADY_EXISTS";
				// if ((await client.execute(QUERIES.checkIfUniqueEmail, [user.email])).rowLength !== 0) throw "USER_ALREADY_EXISTS";

				await client.execute(QUERIES.newUser, [user.username, user.email, user.password]);

				return {

					error: null,
					successful: true
	
				}

			} catch (e) {

				return {

					error: e,
					successful: false
	
				}

			}
			
		}

	},

	User: {

		

	}

}

const server = new ApolloServer({ typeDefs, resolvers });

const app = express();

app.use(cors());

app.use((req, res, next) => {

	// if (rreq.header("X-Authorization") !== "myFavoriteAPIKey") {

	// 	res.end();
	// 	return;

	// }

	next();

});

server.applyMiddleware({ app });

const port = 4000;

app.listen({ port }, () =>

	console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)

);
